# GitLab CI: Artifacts
([Source](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/))

---

## Artifacts
Les artefacts sont un peu comme du cache mais ils peuvent être récupérés depuis un autre stage. Comme pour le cache il faut définir une liste de fichiers ou/et répertoires qui seront sauvegardés par GitLab. Les fichiers sont sauvegardés uniquement si le job réussit.

Nous y retrouvons cinq sous-directives possibles :

- *paths* : obligatoire, elle permet de spécifier la liste des fichiers et/ou dossiers à mettre en artifact

- *name*: facultative, elle permet de donner un nom à l’artifact. Par défaut elle sera nommée artifacts.zip
untracked : facultative, elle permet d’ignorer les fichiers définis dans le fichier .gitignore
- *when* : facultative, elle permet de définir quand l’artifact doit être créé. Trois choix possibles on_success, on_failure, always. La valeur on_success est la valeur par défaut.
- *expire_in* : facultative, elle permet de définir un temps d’expiration

``` yaml
stages:
- build
- test
- deploy 

build:
  stage: build
  script:
  - "echo build_stage: $CI_BUILD_ID >> build"
  artifacts:
    paths:
    - build

test1:
  stage: test
  script:
  - "echo test_stage 1: $CI_BUILD_ID >> test1"
  artifacts:
    paths:
    - test1

test2:
  stage: test
  script:
  - "echo test_stage 2: $CI_BUILD_ID >> test2"
  artifacts:
    paths:
    - test2

deploy:
  stage: deploy
  script:
  - ls -al *
  - cat build
  - cat test1
  - cat test2
``` 
